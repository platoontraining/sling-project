/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 ******************************************************************************/
package org.apache.sling.scripting.sightly.apps.components.podcasts;

import java.io.PrintWriter;
import java.util.Collection;
import javax.script.Bindings;

import org.apache.sling.scripting.sightly.java.compiler.RenderUnit;
import org.apache.sling.scripting.sightly.render.RenderContext;



public final class add_html extends RenderUnit {

    @Override
    protected final void render(PrintWriter out,
                                Bindings bindings,
                                Bindings arguments,
                                RenderContext renderContext) {
// Main Template Body -----------------------------------------------------------------------------

out.write("<!DOCTYPE html>\n<html>\n\t<head>");
{
    Object var_includedresult0 = renderContext.call("include", "/apps/components/header.html", obj());
    out.write(renderContext.getObjectModel().toString(var_includedresult0));
}
out.write("</head>\n<body>\n\t<div class=\"container\">\n\t\t<h1>Add A New Podcast Page</h1>\n\t\t<form method=\"post\" action=\"/content/podcasts\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label for=\"podcastname\">Podcast Name</label>\n\t\t\t\t<input type=\"text\" name=\"jcr:title\" class=\"form-control\" id=\"podcastname\" placeholder=\"Podcast Name\">\n\t\t\t</div>\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<label for=\"podcastdesrciption\">Podcast Description</label>\n\t\t\t\t<textarea class=\"form-control\" rows=\"3\" name=\"jcr:description\" id=\"podcastdescription\"></textarea>\n\t\t\t</div>\n\t\t\t<input type=\"hidden\" name=\"sling:resourceType\" value=\"components/podcast\">\n\t\t\t<div class=\"form-group\">\n\t\t\t\t<button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</body>\n</html>");


// End Of Main Template Body ----------------------------------------------------------------------
    }



    {
//Sub-Templates Initialization --------------------------------------------------------------------



//End of Sub-Templates Initialization -------------------------------------------------------------
    }

}

